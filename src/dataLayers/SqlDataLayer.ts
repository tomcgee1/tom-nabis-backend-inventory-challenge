import { snakeCase } from "lodash";
import {
  formatSqlValue,
  getUpdateForSkuBatchRecord,
  queryExec,
} from "../db/sql.util";
import {
  RecordWithWMS,
  inventoryUpdate,
  skuBatchUpdate,
} from "src/interfaces.util";

export class SqlDataLayer {
  /* Insert  */

  // Should I actually rename this to inventory?
  // Do I also need to insert to the inventory_aggregate here?
  public FormatInserts(inserts: RecordWithWMS[]): string[] {
    return inserts.map(
      (record: RecordWithWMS): string =>
        `insert into test_table (col_1, col_2) values (${record.skuId}, ${record.skuBatchId})`
    );
  }

  public async Insert(inserts: RecordWithWMS[]): Promise<void> {
    const sqlInserts = this.FormatInserts(inserts);
    return await queryExec({}, sqlInserts);
  }

  /* Update */

  public FormatUpdates(deltas: skuBatchUpdate[]): string[] {
    // convert updates to sql and push updates
    return deltas.flatMap((delta) => {
      const updatesToMake = delta.updates
        .map(
          (ud: inventoryUpdate) =>
            `${snakeCase(ud.field)} = ${formatSqlValue(ud.newValue)}`
        )
        .join("; ");

      // Is it not possible for the SKU_batch to have any warehouse unique properties?

      // What's the point of inventory_aggregate and inventory if all the records are the same
      // besides warehouse? Should there just be a mapping from inventory_aggregate to warehouse?

      // Unit Of Measure quantity for example. Shouldn't this be location specific in theory?

      // or is a an 'each' and a 'pallet' a different Sku_batch_id even if everything else is the same?
      // or is unit_of_measure a weight referencing how many grams to add of this sku for this product?

      // would depend if you copack or only receive/ship finished products I guess.

      // We are duping Updates to the Inventory_Aggregate
      // it should be reduced by SkuBatch
      return [
        getUpdateForSkuBatchRecord(
          "inventory",
          updatesToMake,
          delta.skuBatchId
        ),
        getUpdateForSkuBatchRecord(
          "inventory_aggregate",
          updatesToMake,
          delta.skuBatchId
        ),
      ];
    });
  }

  public async Update(deltas: skuBatchUpdate[]): Promise<void> {
    const sqlUpdates = this.FormatUpdates(deltas);
    return await queryExec({}, sqlUpdates);
  }
}
