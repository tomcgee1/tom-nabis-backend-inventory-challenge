import { RecordWithWMS, skuBatchUpdate } from "src/interfaces.util";
import { SqlDataLayer } from "./SqlDataLayer";
import { HttpDataLayer } from "./HttpDataLayer";

export type DataLayerType = "SQL" | "HTTP";

interface IDataLayer {
  Insert: (inserts: RecordWithWMS[]) => Promise<void>,
  Update: (deltas: skuBatchUpdate[]) => Promise<void>,
}
export function getDataLayer(dataLayerType: DataLayerType): IDataLayer{
  if (dataLayerType === "SQL") {
    return new SqlDataLayer();
  } else if (dataLayerType === "HTTP") {
    return new HttpDataLayer();
  }
  throw new Error('Data Layer Not Specified or Not Implemented')
}

