import { getDataLayer } from "./DataLayerFactory";
import { HttpDataLayer } from "./HttpDataLayer";
import { SqlDataLayer } from "./SqlDataLayer";

describe("DataLayerFactory", () => {
    beforeEach(() => {
      jest.resetAllMocks();
    });
  
    describe(".getDataLayer", () => {
      it("should return a SQLDataLayer if DataLayerType is SQL", async () => {
  
        expect(
        getDataLayer("SQL")
        ).toBeInstanceOf(SqlDataLayer);
      });

      it("should return a SQLDataLayer if DataLayerType is SQL", async () => {
  
        expect(
        getDataLayer("HTTP")
        ).toBeInstanceOf(HttpDataLayer);
      });
    })
});