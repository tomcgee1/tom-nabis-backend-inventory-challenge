import { HttpHelperClient } from "../HttpHelperClient";
import { RecordWithWMS, skuBatchUpdate } from "src/interfaces.util";

export class HttpDataLayer {
  client: HttpHelperClient;
  constructor() {
    //Fetch API Key and BaseURL for env here (or in a better AppSettings Util, etc.)
    this.client = new HttpHelperClient(
      "https://local-inventory.nabis.dev/v1/",
      "secret_password"
    );
  }

  /* Insert  */
  public async Insert(inserts: RecordWithWMS[]): Promise<void> {
    const promises = inserts.flatMap((insert) => {
      return [
        this.client.Post("/inventory", {
          ...insert,
          warehouseId: insert.warehouseId,
        }),
        // inserting into the inventory_aggregate table as well just to be safe.
        // maybe whatever is inserting into the App DB is already doing this since it doesn't need to know
        // about the warehouses, but it's easy to delete.
        this.client.Post("/inventory-aggregate", {
          ...insert,
          warehouseId: undefined,
        }),
      ];
    });
    let resolved = await Promise.allSettled(promises);
    let successfulInserts: PromiseFulfilledResult<Response>[] = [];
    let failedInserts: PromiseRejectedResult[] = [];
    resolved.forEach((resolution) => {
      if (resolution.status === "fulfilled") {
        successfulInserts.push(resolution);
      } else {
        // make sure the reason includes an identifying Id
        failedInserts.push(resolution);
      }
    });
    //hooray! Handle the success and failures however you'd like from here.
    let results = { success: successfulInserts, failure: failedInserts };
  }

  /* Update */
  public async Update(deltas: skuBatchUpdate[]): Promise<void> {
    const promises = deltas.flatMap((delta) => {
      let requestBody: { [k: string]: any } = {
        skuBatchId: delta.skuBatchId,
        skuId: delta.skuId,
        warehouseId: delta.wmsId,
      };
      //dynamically add the new values as keys on the requestBody
      delta.updates.forEach((update) => {
        let fieldToUpdate: string = update.field;
        let newVal: string | number | boolean | null = update.newValue;
        requestBody[fieldToUpdate] = newVal;
      });

      return [
        this.client.Put("/inventory", { ...requestBody }),
        this.client.Put("/inventory-aggregate", {
          ...requestBody,
          warehouseId: undefined, //remove the WarehouseId
        }),
      ];
    });
    let resolved = await Promise.allSettled(promises);
    let successfulInserts: PromiseFulfilledResult<Response>[] = [];
    let failedInserts: PromiseRejectedResult[] = [];
    resolved.forEach((resolution) => {
      if (resolution.status === "fulfilled") {
        successfulInserts.push(resolution);
      } else {
        // make sure the reason includes an identifying Id
        failedInserts.push(resolution);
      }
    });
    //hooray! Handle the success and failures however you'd like from here.
    let results = { success: successfulInserts, failure: failedInserts };
  }
}
