export class HttpHelperClient {
    private apiKey: string;
    private baseUrl: string;
    constructor(apiKey: string, baseUrl: string) {
        this.apiKey = apiKey;
        this.baseUrl = baseUrl;
    }

    public async Post(path: string, body: object = {}): Promise<Response> {
        return await fetch(`${this.baseUrl}${path}`, {
            method: "POST",
            cache: "no-cache",
            headers: {
                "Content-Type": "application/json",
                "x-api-key": this.apiKey
            },
            body: JSON.stringify(body)
        });
    }

    
    public async Put(path: string, body: object = {}): Promise<Response> {
        return await fetch(`${this.baseUrl}${path}`, {
            method: "PUT",
            cache: "no-cache",
            headers: {
                "Content-Type": "application/json",
                "x-api-key": this.apiKey
            },
            body: JSON.stringify(body)
        });
    }
}