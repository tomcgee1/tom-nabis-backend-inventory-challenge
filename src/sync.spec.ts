import {
  findChangesBetweenDatasets,
  findDeltas,
  getDeltas,
  makeWarehouseRecordsForSkuBatchRecord,
  skuBatchToInserts,
} from "./sync";
import {
  WMSWarehouseMeta,
  skuBatchUpdate,
  SkuBatchToSkuId,
  SkuBatchData,
} from "./interfaces.util";
import { SqlDataLayer } from "./dataLayers/SqlDataLayer";

describe("sync", () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe(".makeWarehouserecordsForSkuBatchRecord", () => {
    it("should map each sku to each provided Warehouse", async () => {
      let skuBatchRecord = {
        skuBatchId: "tom-test-skuBatchId-1",
        skuId: "tom-test-skuId-1",
        quantityPerUnitOfMeasure: 12,
        wmsId: 1,
        isArchived: false,
        isDeleted: false,
      };
      let warehouseRecords: WMSWarehouseMeta[] = [
        { warehouseId: "tom-test-warehouse-1" },
        { warehouseId: "tom-test-warehouse-2" },
      ];

      expect(
        makeWarehouseRecordsForSkuBatchRecord(skuBatchRecord, warehouseRecords)
      ).toStrictEqual([
        {
          skuBatchId: skuBatchRecord.skuBatchId,
          skuId: skuBatchRecord.skuId,
          wmsId: skuBatchRecord.wmsId,
          quantityPerUnitOfMeasure: skuBatchRecord.quantityPerUnitOfMeasure,
          isArchived: skuBatchRecord.isArchived,
          isDeleted: skuBatchRecord.isDeleted,
          warehouseId: warehouseRecords[0].warehouseId,
        },
        {
          skuBatchId: skuBatchRecord.skuBatchId,
          skuId: skuBatchRecord.skuId,
          wmsId: skuBatchRecord.wmsId,
          quantityPerUnitOfMeasure: skuBatchRecord.quantityPerUnitOfMeasure,
          isArchived: skuBatchRecord.isArchived,
          isDeleted: skuBatchRecord.isDeleted,
          warehouseId: warehouseRecords[1].warehouseId,
        },
      ]);
    });

    it("should return an empty array for no warehouses", async () => {
      let skuBatchRecord = {
        skuBatchId: "tom-test-skuBatchId-1",
        skuId: "tom-test-skuId-1",
        quantityPerUnitOfMeasure: 12,
        wmsId: 1,
        isArchived: false,
        isDeleted: false,
      };
      let warehouseRecords: WMSWarehouseMeta[] = [];

      expect(
        makeWarehouseRecordsForSkuBatchRecord(skuBatchRecord, warehouseRecords)
      ).toStrictEqual([]);
    });

    it("No Sku Provided returns a crazy wacky object with 1 for the QuantityPerUnitOfMeasure", async () => {
      let warehouseRecords: WMSWarehouseMeta[] = [
        { warehouseId: "tom-test-warehouse-1" },
      ];

      expect(
        makeWarehouseRecordsForSkuBatchRecord(
          {} as SkuBatchToSkuId,
          warehouseRecords
        )
      ).toStrictEqual([
        {
          skuBatchId: undefined,
          skuId: undefined,
          wmsId: undefined,
          quantityPerUnitOfMeasure: 1,
          isArchived: undefined,
          isDeleted: undefined,
          warehouseId: warehouseRecords[0].warehouseId,
        },
      ]);
    });

    it("should default quantityPerUnitOfMeasure to 1 if null", async () => {
      let skuBatchRecord = {
        skuBatchId: "tom-test-skuBatchId-1",
        skuId: "tom-test-skuId-1",
        wmsId: 1,
        isArchived: false,
        isDeleted: false,
      };
      let warehouseRecords: WMSWarehouseMeta[] = [
        { warehouseId: "tom-test-warehouse-1" },
        { warehouseId: "tom-test-warehouse-2" },
      ];

      expect(
        makeWarehouseRecordsForSkuBatchRecord(
          skuBatchRecord as SkuBatchToSkuId,
          warehouseRecords
        )
      ).toStrictEqual([
        {
          skuBatchId: skuBatchRecord.skuBatchId,
          skuId: skuBatchRecord.skuId,
          wmsId: skuBatchRecord.wmsId,
          quantityPerUnitOfMeasure: 1,
          isArchived: skuBatchRecord.isArchived,
          isDeleted: skuBatchRecord.isDeleted,
          warehouseId: warehouseRecords[0].warehouseId,
        },
        {
          skuBatchId: skuBatchRecord.skuBatchId,
          skuId: skuBatchRecord.skuId,
          wmsId: skuBatchRecord.wmsId,
          quantityPerUnitOfMeasure: 1,
          isArchived: skuBatchRecord.isArchived,
          isDeleted: skuBatchRecord.isDeleted,
          warehouseId: warehouseRecords[1].warehouseId,
        },
      ]);
    });
  });

  describe(".skuBatchToInserts", () => {
    it("should return a list of inserts", async () => {
      const data = [
        {
          skuBatchId: "sku-batch-id-1",
          skuId: "sku-id-1",
          quantityPerUnitOfMeasure: 25,
        },
        {
          skuBatchId: "sku-batch-id-2",
          skuId: "sku-id-1",
          quantityPerUnitOfMeasure: 25,
        },
        {
          skuBatchId: "sku-batch-id-3",
          skuId: "sku-id-2",
          quantityPerUnitOfMeasure: 1,
        },
      ];

      const sqlDataLayer = new SqlDataLayer();

      expect(
        sqlDataLayer.FormatInserts(await skuBatchToInserts(data.map((d) => d.skuBatchId)))
      ).toStrictEqual([
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-1)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-1)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-1)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-1)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-2)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-2)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-2)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-2)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-3)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-3)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-3)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-3)",
      ]);
    });
  });

  describe(".getDeltas", () => {
    it("should find deltas", async () => {
      // example of the data below:
      // skuBatchIds = ['sku-batch-id-1', 'sku-batch-id-2', 'sku-batch-id-3', 'sku-batch-id-4'];
      // appSkuBatchIds = [...skuBatchIds, 'sku-batch-id-5', 'sku-batch-id-6']; // 5 and 6 are new
      await expect(getDeltas()).resolves.toStrictEqual([
        "sku-batch-id-5",
        "sku-batch-id-6",
      ]);
    });

    it("should find deltas if no delta", async () => {
      await expect(
        getDeltas([{ id: "test-1" }], [{ skuBatchId: "test-1" }])
      ).resolves.toStrictEqual([]);
    });

    it("should not find the reverse deltas (in inventory but not app)", async () => {
      await expect(
        getDeltas(
          [{ id: "test-1" }],
          [{ skuBatchId: "test-1" }, { skuBatchId: "test-2" }]
        )
      ).resolves.toStrictEqual([]);
    });

    it("should not find the reverse deltas even if other deltas", async () => {
      await expect(
        getDeltas(
          [{ id: "test-1" }, { id: "test-3" }],
          [{ skuBatchId: "test-1" }, { skuBatchId: "test-2" }]
        )
      ).resolves.toStrictEqual(["test-3"]);
    });
  });

  describe(".findDelta", () => {
    it("should pick up changes to quantityPerUnitOfMeasure", async () => {
      const appData = [
        {
          skuBatchId: "1",
          skuId: "1",
          wmsId: "1",
          quantityPerUnitOfMeasure: 5,
          isArchived: false,
          isDeleted: false,
        },
      ];

      const inventoryData = [
        {
          skuBatchId: "1",
          skuId: "1",
          wmsId: "1",
          quantityPerUnitOfMeasure: 10,
          isArchived: false,
          isDeleted: false,
        },
      ];

      const deltas: skuBatchUpdate[] = findDeltas(appData, inventoryData);
      expect(deltas.length).toBe(1);
      expect(deltas[0].updates.length).toBe(1);
      expect(deltas[0].updates[0].field).toBe("quantityPerUnitOfMeasure");
      expect(deltas[0].updates[0].newValue).toBe(5);
    });

    it("should not change the skuId if already set", async () => {
      const appData = [
        {
          skuBatchId: "1",
          skuId: "1",
          wmsId: "1",
          quantityPerUnitOfMeasure: 10,
          isArchived: false,
          isDeleted: false,
        },
      ];

      const inventoryData = [
        {
          skuBatchId: "1",
          skuId: "1",
          wmsId: "1",
          quantityPerUnitOfMeasure: 10,
          isArchived: false,
          isDeleted: false,
        },
      ];

      const deltas: skuBatchUpdate[] = findDeltas(appData, inventoryData);
      expect(deltas.length).toBe(0);
    });

    it("should pick up change to skuId if not set", async () => {
      const appData = [
        {
          skuBatchId: "1",
          skuId: "1",
          wmsId: "1",
          quantityPerUnitOfMeasure: 10,
          isArchived: false,
          isDeleted: false,
        },
      ];

      const inventoryData = [
        {
          skuBatchId: "1",
          wmsId: "1",
          skuId: null,
          quantityPerUnitOfMeasure: 10,
          isArchived: false,
          isDeleted: false,
        },
      ];

      const deltas: skuBatchUpdate[] = findDeltas(
        appData,
        inventoryData as SkuBatchData[]
      );
      expect(deltas.length).toBe(1);
      expect(deltas[0].updates.length).toBe(1);
      expect(deltas[0].updates[0].field).toBe("skuId");
      expect(deltas[0].updates[0].newValue).toBe("1");
    });

    it("should pick up change to wmsId", async () => {
      const appData = [
        {
          skuBatchId: "1",
          skuId: "1",
          wmsId: "1",
          quantityPerUnitOfMeasure: 10,
          isArchived: false,
          isDeleted: false,
        },
      ];

      const inventoryData = [
        {
          skuBatchId: "1",
          wmsId: "4",
          skuId: "1",
          quantityPerUnitOfMeasure: 10,
          isArchived: false,
          isDeleted: false,
        },
      ];

      const deltas: skuBatchUpdate[] = findDeltas(
        appData,
        inventoryData as SkuBatchData[]
      );
      expect(deltas.length).toBe(1);
      expect(deltas[0].updates.length).toBe(1);
      expect(deltas[0].updates[0].field).toBe("wmsId");
      expect(deltas[0].updates[0].newValue).toBe("1");
    });
    it("should pick up changes to all Fields", async () => {
      const appData = [
        {
          skuBatchId: "1",
          skuId: "1",
          wmsId: "1",
          quantityPerUnitOfMeasure: 10,
          isArchived: false,
          isDeleted: true,
        },
      ];

      const inventoryData = [
        {
          skuBatchId: "1",
          wmsId: "2",
          skuId: null,
          quantityPerUnitOfMeasure: 11,
          isArchived: true,
          isDeleted: false,
        },
      ];

      const deltas: skuBatchUpdate[] = findDeltas(
        appData,
        inventoryData as SkuBatchData[]
      );
      expect(deltas.length).toBe(1);
      expect(deltas[0].updates.length).toBe(5);
      expect(deltas[0].updates).toContainEqual({
        field: "skuId",
        newValue: "1",
      });
      expect(deltas[0].updates).toContainEqual({
        field: "wmsId",
        newValue: "1",
      });
      expect(deltas[0].updates).toContainEqual({
        field: "quantityPerUnitOfMeasure",
        newValue: 10,
      });
      expect(deltas[0].updates).toContainEqual({
        field: "isArchived",
        newValue: false,
      });
      expect(deltas[0].updates).toContainEqual({
        field: "isDeleted",
        newValue: true,
      });
    });

    it("should pick up changes to all Fields on multiple objects", async () => {
      const appData = [
        {
          skuBatchId: "1",
          skuId: "1",
          wmsId: "1",
          quantityPerUnitOfMeasure: 10,
          isArchived: false,
          isDeleted: true,
        },
        {
          skuBatchId: "2",
          skuId: "2",
          wmsId: "2",
          quantityPerUnitOfMeasure: 12,
          isArchived: false,
          isDeleted: true,
        },
      ];

      const inventoryData = [
        {
          skuBatchId: "1",
          wmsId: "2",
          skuId: null,
          quantityPerUnitOfMeasure: 11,
          isArchived: true,
          isDeleted: false,
        },
        {
          skuBatchId: "2",
          wmsId: "5",
          skuId: null,
          quantityPerUnitOfMeasure: 11,
          isArchived: true,
          isDeleted: false,
        },
      ];

      const deltas: skuBatchUpdate[] = findDeltas(
        appData,
        inventoryData as SkuBatchData[]
      );
      expect(deltas.length).toBe(2);
      expect(deltas[0].updates.length).toBe(5);
      expect(deltas[0].updates).toContainEqual({
        field: "skuId",
        newValue: "1",
      });
      expect(deltas[0].updates).toContainEqual({
        field: "wmsId",
        newValue: "1",
      });
      expect(deltas[0].updates).toContainEqual({
        field: "quantityPerUnitOfMeasure",
        newValue: 10,
      });
      expect(deltas[0].updates).toContainEqual({
        field: "isArchived",
        newValue: false,
      });
      expect(deltas[0].updates).toContainEqual({
        field: "isDeleted",
        newValue: true,
      });
      expect(deltas[1].updates.length).toBe(5);
      expect(deltas[1].updates).toContainEqual({
        field: "skuId",
        newValue: "2",
      });
      expect(deltas[1].updates).toContainEqual({
        field: "wmsId",
        newValue: "2",
      });
      expect(deltas[1].updates).toContainEqual({
        field: "quantityPerUnitOfMeasure",
        newValue: 12,
      });
      expect(deltas[1].updates).toContainEqual({
        field: "isArchived",
        newValue: false,
      });
      expect(deltas[1].updates).toContainEqual({
        field: "isDeleted",
        newValue: true,
      });
    });

    it("should find changes between datasets", async () => {
      const changesBetweendDataSets = await findChangesBetweenDatasets();

      //replace this with a call to each factory method
      let updater = new SqlDataLayer();
      expect(updater.FormatUpdates(changesBetweendDataSets)).toStrictEqual([
        "update inventory set is_deleted = true where sku_batch_id = 'sku-batch-id-5'",
        "update inventory_aggregate set is_deleted = true where sku_batch_id = 'sku-batch-id-5'",
        "update inventory set is_archived = true where sku_batch_id = 'sku-batch-id-6'",
        "update inventory_aggregate set is_archived = true where sku_batch_id = 'sku-batch-id-6'",
      ]);
    });
  });

  describe(".makeUpdates", () => {
    it("should create a list of string sql updates based on a update delta with boolean", async () => {
      let updater = new SqlDataLayer();
      let delta = [{
        skuBatchId: "1",
        skuId: "1",
        wmsId: "1",
        updates: [{ field: "isDeleted", newValue: true }],
      }];
      expect(updater.FormatUpdates(delta)).toStrictEqual([
        "update inventory set is_deleted = true where sku_batch_id = '1'",
        "update inventory_aggregate set is_deleted = true where sku_batch_id = '1'",
      ]);
    });

    it("should create a list of string sql updates based on a update with string", async () => {
      let updater = new SqlDataLayer();
      let delta = [{
        skuBatchId: "1",
        skuId: "1",
        wmsId: "1",
        updates: [{ field: "wmsId", newValue: "8" }],
      }];
      expect(updater.FormatUpdates(delta)).toStrictEqual([
        "update inventory set wms_id = '8' where sku_batch_id = '1'",
        "update inventory_aggregate set wms_id = '8' where sku_batch_id = '1'",
      ]);
    });

    it("should create a list of string sql updates based on a update with number", async () => {
      let updater = new SqlDataLayer();
      let delta = [{
        skuBatchId: "1",
        skuId: "1",
        wmsId: "1",
        updates: [{ field: "quantityPerUnitOfMeasure", newValue: 123 }],
      }];
      expect(updater.FormatUpdates(delta)).toStrictEqual([
        "update inventory set quantity_per_unit_of_measure = 123 where sku_batch_id = '1'",
        "update inventory_aggregate set quantity_per_unit_of_measure = 123 where sku_batch_id = '1'",
      ]);
    });
    it("should create a list of string sql updates based on a update with NULL", async () => {
      let updater = new SqlDataLayer();
      let delta = [{
        skuBatchId: "1",
        skuId: "1",
        wmsId: "1",
        updates: [{ field: "skuId", newValue: null }],
      }];
      expect(updater.FormatUpdates(delta)).toStrictEqual([
        "update inventory set sku_id = null where sku_batch_id = '1'",
        "update inventory_aggregate set sku_id = null where sku_batch_id = '1'",
      ]);
    });
  });
});
