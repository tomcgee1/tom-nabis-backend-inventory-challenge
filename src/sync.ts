import {
  WMSWarehouseMeta,
  inventoryUpdate,
  RecordWithWMS,
  SkuBatchData,
  SkuBatchToSkuId,
  skuBatchUpdate,
} from "./interfaces.util";
import {
  appData,
  appSkuBatchData,
  appSkuBatchDataForSkuBatchIds,
  skuBatchIdsFromInventoryDb as skuBatchIdsFromInventoryDb_raw,
  skuBatchIdsFromAppDb as skuBatchIdsFromAppDb_raw,
  warehouseData as warehouseDataFromWMS,
} from "./db/data";
import { DataLayerType, getDataLayer } from "./dataLayers/DataLayerFactory";

const logger = console;
const methodToUse: DataLayerType = 'SQL'; //Set to 'HTTP' for REST, but endpoint 404s

/**
 * Create a list of records for a skuBatch record that maps skuBatchId + warehouseId
 * @param skuBatchRecord
 */
export const makeWarehouseRecordsForSkuBatchRecord = (
  skuBatchRecord: SkuBatchToSkuId,
  warehouseData: WMSWarehouseMeta[]
): RecordWithWMS[] => {
  return warehouseData.map(
    (warehouse: WMSWarehouseMeta): RecordWithWMS => ({
      skuBatchId: skuBatchRecord.skuBatchId,
      skuId: skuBatchRecord.skuId,
      wmsId: skuBatchRecord.wmsId,
      quantityPerUnitOfMeasure: skuBatchRecord.quantityPerUnitOfMeasure ?? 1,
      isArchived: skuBatchRecord.isArchived,
      isDeleted: skuBatchRecord.isDeleted,
      warehouseId: warehouse.warehouseId,
    })
  );
};

/**
 * Converts a list of skuBatchIds from the app db into an insert to inventory.
 * @param skuBatchIdsToInsert
 */
export async function skuBatchToInserts(
  skuBatchIdsToInsert: string[]
): Promise<RecordWithWMS[]> {
  const badSkuBatchCounter = { count: 0 };

  // create our inserts
  const inserts: RecordWithWMS[] = skuBatchIdsToInsert
    .reduce((arr: RecordWithWMS[], skuBatchId: string): RecordWithWMS[] => {
      // Change != to === since we want to find the MATCHING skuAppId from the AppData and insert it
      // into the Inventory Db
      const skuBatchRecordFromAppDb: SkuBatchToSkuId | undefined = appData.find(
        (skuBatchToSkuId: SkuBatchToSkuId): boolean =>
          skuBatchToSkuId.skuBatchId === skuBatchId
      );

      if (!skuBatchRecordFromAppDb) {
        logger.error(
          `no records found in app SkuBatch [skuBatchId=${skuBatchId}}]`
        );
        badSkuBatchCounter.count += 1;
        return arr;
      }

      arr.push(
        ...makeWarehouseRecordsForSkuBatchRecord(
          skuBatchRecordFromAppDb,
          warehouseDataFromWMS
        )
      );
      return arr;
    }, [])

  logger.log(
    `created inserts [count=${inserts.length}, badSkuBatchRecordCount=${badSkuBatchCounter.count}]`
  );

  return inserts;
}

/**
 * Diffs the inventory between app SkuBatch and inventory to determine
 * what we need to copy over.
 */
export async function getDeltas(
  skuBatchIdsFromAppDb: { id: string }[] = skuBatchIdsFromAppDb_raw,
  skuBatchIdsFromInventoryDb: {
    skuBatchId: string;
  }[] = skuBatchIdsFromInventoryDb_raw
): Promise<string[]> {
  try {
    const inventorySkuBatchIds: Set<string> = new Set<string>(
      skuBatchIdsFromInventoryDb.map(
        (r: { skuBatchId: string }) => r.skuBatchId
      )
    );

    // return keys that are present in the App DB but not the inventory Db yet
    // Assumption is things can only get added from the App Db and never deleted
    // Seems to be soft deletion everywhere
    return [
      ...new Set<string>(skuBatchIdsFromAppDb.map((r: { id: string }) => r.id)),
    ].filter((x: string) => !inventorySkuBatchIds.has(x));
  } catch (err) {
    logger.error("error querying databases for skuBatchIds");
    logger.error(err);
    throw err;
  }
}


/**
 * Finds the deltas between two lists of SkuBatchData
 * @param appSkuBatchData
 * @param inventorySkuBatchData
 */
export const findDeltas = (
  appSkuBatchData: SkuBatchData[],
  inventorySkuBatchData: SkuBatchData[]
): skuBatchUpdate[] => {
  logger.log(
    "finding data changes between inventory and app SkuBatch datasets"
  );

  return appSkuBatchData
    .map((appSbd: SkuBatchData) => {
      const inventoryRecord: SkuBatchData | undefined =
        inventorySkuBatchData.find(
          (r: SkuBatchData): boolean => r.skuBatchId == appSbd.skuBatchId
        );

      if (!inventoryRecord) {
        // if we cannot find the matching record, we have a problem
        logger.warn(
          `cannot find matching inventory record! [skuBatchId=${appSbd.skuBatchId}]`
        );
        // instead of throwing an error, return empty update array which will
        // get filtered out at the end of this chain
        return { skuBatchId: "", skuId: "", wmsId: "", updates: [] };
      }

      // go through each key and see if it is different, if so, track it
      const updates: inventoryUpdate[] = Object.keys(inventoryRecord)
        .filter((k: string) => !["skuBatchId"].includes(k))
        .reduce(
          (
            recordUpdates: inventoryUpdate[],
            key: string
          ): inventoryUpdate[] => {
            const inventoryValue =
              inventoryRecord[key as keyof typeof inventoryRecord];
            const appValue = appSbd[key as keyof typeof appSbd];

            if (key == "skuId" && inventoryValue != null) {
              // if the key is skuId and the current value is set, we won't update
              return recordUpdates;
            }

            if (inventoryValue != appValue) {
              recordUpdates.push({ field: key, newValue: appValue });
            }

            return recordUpdates;
          },
          [] as inventoryUpdate[]
        );

      return {
        skuBatchId: inventoryRecord.skuBatchId,
        skuId: appSbd.skuId,
        wmsId: appSbd.wmsId,
        updates,
      };
    })
    .filter((sbu: skuBatchUpdate) => sbu.updates.length > 0);
};

/**
 * Finds changes in data between the app SkuBatch+Sku and inventory tables
 */
export async function findChangesBetweenDatasets(): Promise<skuBatchUpdate[]> {
  logger.log(
    "finding app SkuBatch data that has changed and <> the inventory data"
  );

  const updates: skuBatchUpdate[] = await [appSkuBatchData].reduce(
    async (
      accumPromise: Promise<skuBatchUpdate[]>,
      inventorySkuBatchData: SkuBatchData[]
    ) => {
      const accum: skuBatchUpdate[] = await accumPromise;
      const skuBatchIds: string[] = inventorySkuBatchData.map(
        (sbd: SkuBatchData) => sbd.skuBatchId
      );

      logger.log(
        `querying Logistics.SkuBatch for data [skuBatchIdCount=${skuBatchIds.length}]`
      );
      // fetch SkuBatch+Sku data from the app database
      const appSkuBatchData: SkuBatchData[] = appSkuBatchDataForSkuBatchIds;

      // if we have a count mismatch, something is wrong, and we should log out a warning
      if (appSkuBatchData.length != inventorySkuBatchData.length) {
        const appSkuBatchs = new Set(appSkuBatchData.map((x) => x.skuBatchId));
        const inventorySkuBatchs = new Set(
          inventorySkuBatchData.map((x) => x.skuBatchId)
        );
        const missingAppSkuBatchs: string[] = [];
        inventorySkuBatchs.forEach((x) => {
          if (!appSkuBatchs.has(x)) {
            missingAppSkuBatchs.push(x);
          }
        });
        logger.warn(
          "skuBatchIds that exist in Inventory but not in App",
          missingAppSkuBatchs.join(",")
        );
      }

      // push our new sql updates into the accumulator list
      const ds: skuBatchUpdate[] = findDeltas(
        appSkuBatchData,
        inventorySkuBatchData
      );

      accum.push(...ds);
      return accum;
    },
    Promise.resolve([] as skuBatchUpdate[])
  );

  logger.log(`built updates [count=${updates.length}]`);

  return updates;
}

/**
 * Updates inventory data from app SkuBatch and Sku
 */
export async function copyMissingInventoryRecordsFromSkuBatch(): Promise<void | Error> {
  logger.log("copying missing inventory records from app Sku/SkuBatch");

  // find out what skuBatchIds don't exist in inventory
  const skuBatchIdsToInsert: string[] = await getDeltas();
  logger.log(
    `copying new skuBatch records... [skuBatchCount=${skuBatchIdsToInsert.length}]`
  );
  try {
    let dataLayer = getDataLayer(methodToUse);
    const inserts = await skuBatchToInserts(skuBatchIdsToInsert);
    await dataLayer.Insert(inserts);
  } catch (err) {
    logger.error(err);
    throw err;
  }

  logger.log("done updating additive data to inventory from app db");
}

/**
 * Pulls inventory and SkuBatch data and finds changes in SkuBatch data
 * that are not in the inventory data.
 */
export async function updateInventoryDeltasFromSkuBatch(): Promise<void> {
  logger.log('updating inventory from deltas in "SkuBatch" data');

  try {
    let dataLayer = getDataLayer(methodToUse);
    const updates: skuBatchUpdate[] = await findChangesBetweenDatasets();
   dataLayer.Update(updates);
  } catch (err) {
    logger.error(err);
    throw err;
  }

  logger.log("done updating inventory from deltas from app db");
}

/**
 * Primary entry point to sync SkuBatch data from the app
 * database over to the inventory database
 */
export async function sync(): Promise<void | Error> {
  try {
    await copyMissingInventoryRecordsFromSkuBatch();
    await updateInventoryDeltasFromSkuBatch();
  } catch (err) {
    logger.error("error syncing skuBatch data");
    return Promise.reject(err);
  }
}
