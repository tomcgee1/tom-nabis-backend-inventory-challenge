import {RecordWithWMS} from "../interfaces.util";

export const getUpdateForSkuBatchRecord = (table: string, updates: string, skuBatchId: string) =>
  `update ${table} set ${updates} where sku_batch_id = '${skuBatchId}'`;

// no op that would take our db connection and execute the list of sql statements
// make sure we're escaping any inputs at the lowest level possible probably in this queryExec function
export const queryExec = (db: any, sql: string[]): Promise<void> => Promise.resolve();

// string -> set x = 'string'
// number -> set x = 123
// boolean -> set x = TRUE
// null -> set x = NULL
const stringCompareConstant = 'string';
export const formatSqlValue = (v: string | number | boolean | null) => {
  if (typeof v == stringCompareConstant) {
    return `'${v}'`
  }
    return v;
};